package pcd.lab03.gui.chrono;

public interface CounterEventListener {
	void counterChanged(CounterEvent ev);
}
